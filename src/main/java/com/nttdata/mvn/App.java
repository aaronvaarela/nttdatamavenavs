package com.nttdata.mvn;

import java.util.Scanner;
import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @author Aaron
 *
 */
public class App {
	/**
	 * Método principal
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Bienvenido al Bootcamp de Java + Microservicios de NTT DATA");
		
		// Clase empleada para pedir input al usuario
		Scanner input = new Scanner(System.in);
		
		salute(input);
		
		input.close();
	}

	/**
	 *  Método para solicitar el nombre al usuario y saludarlo
	 * 
	 * @param input instancia de la clase Scanner
	 */
	public static void salute(Scanner input) {
		boolean validString;
		String name = "";
		String greeting = "Hola ";
		
		do {
			validString = true;
			
			System.out.println("¿Como te llamas?");
			
			// Solicitar al usuario que introduzca un texto
			name = input.nextLine();
			
			if(!StringUtils.isNoneBlank(name)) {
				validString = false;
			}

		} while(!validString);

		// Eliminar espacios en blanco al principio y al final
		name = StringUtils.trim(name);
		
		// Poner la primera letra en mayúscula
		name = StringUtils.capitalize(name);
		
		greeting += name;
		System.out.println(greeting);
	}
}
